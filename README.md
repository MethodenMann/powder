# Gnar Powder

[![MIT license](http://img.shields.io/badge/license-MIT-brightgreen.svg)](http://opensource.org/licenses/MIT)
[![codecov](https://codecov.io/gl/gnaar/powder/branch/master/graph/badge.svg?token=MRowdXaujg)](https://codecov.io/gl/gnaar/powder)
[![pipeline status](https://gitlab.com/gnaar/powder/badges/master/pipeline.svg)](https://gitlab.com/gnaar/powder/commits/master)

**Part of Project Gnar:** &nbsp;[base](https://hub.docker.com/r/gnar/base) &nbsp;•&nbsp; [gear](https://pypi.org/project/gnar-gear) &nbsp;•&nbsp; [piste](https://gitlab.com/gnaar/piste) &nbsp;•&nbsp; [off-piste](https://gitlab.com/gnaar/off-piste) &nbsp;•&nbsp; [edge](https://www.npmjs.com/package/gnar-edge) &nbsp;•&nbsp; [powder](https://gitlab.com/gnaar/powder) &nbsp;•&nbsp; [genesis](https://gitlab.com/gnaar/genesis) &nbsp;•&nbsp; [patrol](https://gitlab.com/gnaar/patrol)

**Get started with Project Gnar on** &nbsp;[![Project Gnar on Medium](https://s3-us-west-2.amazonaws.com/project-gnar/medium-68x20.png)](https://medium.com/@ic3b3rg/project-gnar-d274165793b6)

**Join Project Gnar on** &nbsp;[![Project Gnar on Slack](https://s3-us-west-2.amazonaws.com/project-gnar/slack-69x20.png)](https://join.slack.com/t/project-gnar/shared_invite/enQtNDM1NzExNjY0NjkzLWQ3ZTQyYjgwMjkzNWYxNDJiNTQzODY0ODRiMmZiZjVkYzYyZWRkOWQzNjA0OTk3NWViNWM5YTZkMGJlOGIzOWE)

**Support Project Gnar on** &nbsp;[![Project Gnar on Patreon](https://s3-us-west-2.amazonaws.com/project-gnar/patreon-85x12.png)](https://patreon.com/project_gnar)

Project Gnar is a full stack, turnkey starter web app which includes:

- Sign up email with secure account activation
- Secure login and session management via JWT
- Basic account details (name, address, etc.)
- Password reset email with secure confirmation
- React-based frontend and Python-based microservice backend
- Microservice intra-communication
- SQS message polling and sending
- AWS Cloud-based hosting and Terraform + Kubernetes deployment

A demo site is up at [gnar.ca](https://app.gnar.ca) - you're welcome to create an account and test the workflows.

Gnar Powder is the React-based frontend of Project Gnar, built with:

- React &nbsp;•&nbsp; Redux &nbsp;•&nbsp; Redux Saga &nbsp;•&nbsp; React Router &nbsp;•&nbsp; Material UI &nbsp;•&nbsp; Roboto Font
- Lodash &nbsp;•&nbsp; Moment &nbsp;•&nbsp; Immutable &nbsp;•&nbsp; Classnames &nbsp;•&nbsp; Animate.css &nbsp;•&nbsp; Gnar Edge
- Eslint &nbsp;•&nbsp; Jest &nbsp;•&nbsp; Enzyme &nbsp;•&nbsp; Babel &nbsp;•&nbsp; Webpack &nbsp;•&nbsp; Gitlab CI

## Application Structure

```
+ config
- src
  + __mocks__
  - assets
    + images
    + template
  + css
  - js
    + __mocks__
    + api
    + components
    + errors
    + i18n
    - redux
      + sagas
    + router
    + util
    + views
  ```

### Root Files

- **[.dockerignore](https://docs.docker.com/engine/reference/builder/#dockerignore-file)**: Configured to whitelist only the files Docker needs (`config/*` and `dist/*`) to build the container. This configuration significantly improves the build time.

- **[.eslintignore](https://eslint.org/docs/user-guide/configuring#eslintignore)**: Specifies project assets that should not be linted.

- **[.eslintrc.js](https://eslint.org/docs/user-guide/configuring)**: Configures eslint - see the Eslint section for .

- **[.gitignore](https://git-scm.com/docs/gitignore)**: Specifies intentionally untracked files to ignore.

- **[.gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/)**: Configures Gitlab CI jobs. Runs the tests and [registers the coverage report with codecov](https://codecov.io/gl/gnaar/powder).

- **[.nvmrc](https://github.com/creationix/nvm#nvmrc)**: Specifies the node version to use with [nvm](https://github.com/creationix/nvm) (stable).

- **[.sass-lint.yml](https://github.com/sasstools/sass-lint#configuring)**: Configures sass-lint.

- **[babel.config.js](https://babeljs.io/docs/en/config-files#project-wide-configuration)**: Configures babel:

  - presets
      - [@babel/preset-env](https://babeljs.io/docs/en/babel-preset-env.html): Uses the `browserslist` setting in `package.json` to determine which shims to include in the build
      - [@babel/preset-react](https://babeljs.io/docs/en/babel-preset-react.html): Handles jsx syntax
  - env.production.presets
      - [react-optimize](https://github.com/jamiebuilds/babel-react-optimize): Applies a few jsx optimizations in production
  - plugins
      - [react-hot-loader/babel](https://www.npmjs.com/package/react-hot-loader): Enables hot module reloading in development - used in `src/js/router/app.jsx` and `src/js/redux/configureStore.js`
      - [@babel/transform-runtime](https://babeljs.io/docs/en/babel-plugin-transform-runtime.html): Enables the re-use of Babel's injected helper code to reduce the build size
      - [@babel/plugin-syntax-dynamic-import](https://babeljs.io/docs/en/babel-plugin-syntax-dynamic-import): Enables [dynamic (i.e. async) imports](https://developers.google.com/web/updates/2017/11/dynamic-import)
      - [@babel/plugin-proposal-function-bind](https://babeljs.io/docs/en/next/babel-plugin-proposal-function-bind.html): Enables use of the `::` operator to bind an object to a function
      - [@babel/plugin-proposal-export-default-from](https://babeljs.io/docs/en/next/babel-plugin-proposal-export-default-from.html): Enables export syntax in the form `export x from 'module'`
      - [@babel/plugin-proposal-decorators](https://babeljs.io/docs/en/next/babel-plugin-proposal-decorators.html): Enable the use of decorators to produce higher-order components
      - [@babel/plugin-proposal-class-properties](https://babeljs.io/docs/en/next/babel-plugin-proposal-class-properties.html): Enables the use of class properties to simplify class syntax
      - [module-resolver](module-resolver): Enables the use of aliases to simplify our imports (i.e. replaces relative paths)

- **credit.txt**: Used in production to display a credit message in the console

- **Dockerfile**: Defines Powder's docker image - a very simple nginx server configured to serve our distribution files

- **jest.setup.js**: Defines a few globals and configures enzyme

- **LICENSE**: The [MIT License](https://opensource.org/licenses/MIT)

- **[package-lock.json](https://docs.npmjs.com/files/package-lock.json)**: Describes the exact current npm tree

- **[package.json](https://docs.npmjs.com/files/package.json)**: Powder's npm configuration:

  - [browserslist](https://www.npmjs.com/package/browserslist): Used by babel and the css autoprefixer to determine which shims to include in the build
  - dependencies: The production dependencies bundled with Powder
      - [@material-ui/core](https://www.npmjs.com/package/@material-ui/core): Material UI core components - supports tree-shaking
      - [@material-ui/icons](https://www.npmjs.com/package/@material-ui/icons): Material UI icons - supports tree-shaking
      - [animate.css](https://daneden.github.io/animate.css): Awesome set of CSS animations
      - [classnames](https://github.com/JedWatson/classnames): Handy utility for composing a classNames string
      - [gnar-edge](https://www.npmjs.com/package/gnar-edge): Cool set of utilities
      - [immutable](https://www.npmjs.com/package/immutable): Immutable data structures - used in the store
      - [lodash](https://www.npmjs.com/package/lodash): Mature JS utility library
      - [moment](https://momentjs.com/): Tried and true date manipulation
      - [moment-timezone](https://momentjs.com/timezone): Adds timezone support to moment
      - [prop-types](https://www.npmjs.com/package/prop-types): Runtime type checking for React props and similar objects
      - [react](https://reactjs.org/): React!
      - [react-dom](https://www.npmjs.com/package/react-dom): The entry point of the DOM-related rendering paths
      - [react-google-recaptcha](https://www.npmjs.com/package/react-google-recaptcha): React component for [Google reCAPTCHA v2](https://www.google.com/recaptcha)
      - [react-hot-loader](https://www.npmjs.com/package/react-hot-loader): Tweak React components in real time
      - [react-loadable](https://www.npmjs.com/package/react-loadable): A higher order component for loading components with dynamic imports
      - [react-redux](https://www.npmjs.com/package/react-redux): Official React bindings for Redux
      - [react-router](https://reacttraining.com/react-router/web/guides/philosophy): Declarative routing for React
      - [react-router-dom](https://www.npmjs.com/package/react-router-dom): DOM bindings for React Router
      - [react-router-redux](https://www.npmjs.com/package/react-router-redux): Keeps the router in sync with application state
      - [redux](https://redux.js.org): Predictable state container for JavaScript apps
      - [redux-actions](https://redux-actions.js.org/): [Flux Standard Action](https://github.com/redux-utilities/flux-standard-action) utilities for Redux
      - [redux-devtools-extension](https://github.com/zalmoxisus/redux-devtools-extension): A live-editing time travel environment for Redux
      - [redux-saga](https://redux-saga.js.org): Redux side-effects using generators
      - [roboto-fontface](https://www.npmjs.com/package/roboto-fontface): The [recommended font](https://material-ui.com/style/typography) for Material UI
  - devDependencies: Dependencies only used during development - touching on packages not covered in other sections
      - [@trust/webcrypto](https://www.npmjs.com/package/@trust/webcrypto): Emulates window.crypto in tests
      - [chance](https://www.npmjs.com/package/chance): Adds a bit of randomness to the tests
      - [husky](https://www.npmjs.com/package/husky): Adds a pre-commit hook used to test the build - see `husky` in `package.json`
  - [husky](https://github.com/typicode/husky): Git hooks made easy
      - hooks: Adds a `pre-commit` hook that lints and tests the code
  - [scripts](https://docs.npmjs.com/misc/scripts):
      - `build`: build and analyze the development build
      - `build:prod`: build the production build
      - `build:prod:analyze`: build and analyze the production build
      - `clean`: remove the `dist` folder
      - `lint:js`: lint the js files`
      - `lint:sass`: lint the scss files
      - `lint`: lint the js & scss files
      - `prebuild`: automatically run before any build* script - removes the `dist` folder
      - `prestart`: automatically run before any start* script - removes the `dist` folder
      - `start`: runs the development build using the webpack dev server
      - `start:open`: runs the development build using the webpack dev server and opens a new browser tab
      - `start:prod`: runs the production build using the webpack dev server
      - `start:prod:open`: runs the production build using the webpack dev server and opens a new browser tab
      - `test`: runs the Jest tests
      - `test:debug`: runs the Jest tests in debug mode - opens chrome devtools and pauses on `debugger` commands (flaky, but works)
      - `test:watch`: runs the Jest tests and watches the filesystem for changes


- **[webpack.config.babel.js](https://webpack.js.org/configuration)**: [Webpack config](#webpack-build)

### config

- **default.conf**: Custom nginx config, used by `Dockerfile`

### src/\_\_mocks\_\_

Contains mocks used in the tests. Some of the mocks are directly imported into the test files that use them, others are stubs used by Jest - see `jest.moduleNameMapper` in `jest.config.js`.

### src/assets

Static assets used by the app.

#### src/assets/images

All the images used within the app.

#### src/assets/template

The file used by `HtmlWebpackPlugin` in `webpack.config.babel.js` to create the app's `index.html` page.

### src/css

Static css (i.e. non-scss files) used within the app - non-theme colors (used for the body's linear-gradient) and animations.

### src/js

The app's JavaScript files.

#### src/js/\_\_mocks\_\_

Auto mocks for node modules, respecting the `<rootDir>/src/js` Jest root in `jest.config.js`.

#### src/js/api

The app's api.

Each top level api of the app (e.g. `/user`) should have its own routes file which is registered in `index.js`. The routes are used by the sagas.

- **request.js**: Handles every api request. The default export in the file is a generator function which is intended to be called by a saga generator function.

  Main features of the `request` function:

  - Checks jwt expiration on auth (authenticated) routes, auto logout and error notification if the token has expired.

    ![Session Expired Notification](https://s3-us-west-2.amazonaws.com/powder-readme-assets/session-expired.gif)

  - Maps services to origins in development - origins are defined in `webpack.config.babel.js`
  - Add the app's current jwt token to every auth route.
  - Updates the app's current jwt token using the token provided in the response header (by Gnar Gear) on auth routes.
  - Sets a timeout to check the logged in status (unexpired jwt token) 250 ms after the new jwt token is set to expire - auto logout if the token is expired.
  - Check the response for errors (errors that have been handled by the server return a 200 in the form `{ "error": "Error Message", "traceback": "Error traceback log" }`.
      - All errors are have their human-readable traceback logged at error level to the console, for example:

      ![Piste Error](https://s3-us-west-2.amazonaws.com/powder-readme-assets/piste-error.png)

      - Displays the error message in an error notification unless `toastErrors = false` is specified.
  - Server errors (errors not handled by Flask) have their error message logged at error level and displayed in an error notification (unless `toastErrors = false` is specified).

#### src/js/components

Custom components for the app.

- **header**: App header containing logo - clicking on the logo redirects to `/account` or `/login`, depending on whether or not the user is logged in.

- **overlay**: The overlay used to indicate the app is busy and to prevent interaction with the underlying components.

  ![Login Overlay](https://s3-us-west-2.amazonaws.com/powder-readme-assets/login-overlay.gif)

#### src/js/errors

Custom errors for the app.

#### src/js/i18n

Contains the `Change Language` button and all the translations for the app. The translation files are bundled into separate chunks by webpack and they are loaded asynchronously on demand when a user picks a language with the `Change Language` button. This is achieved by the following line of code in the `setLanguage` function of the `I18nButton` class:

```javascript
import(/* webpackChunkName: "assets/i18n/[request]" */ `./${nextLanguage.toLowerCase()}.json`)
```

Calling `import` as a function triggers webpack's dynamic bundler. The `/* webpackChunkName: "assets/i18n/[request]" */` comment is used by webpack to set the name of the chunk. The `request` refers to the `output.chunkFilename` setting in the webpack config.

#### src/js/redux

The redux actions, reducers, and sagas for the app.

- **actions**: The actions are payloads of data that are sent to the store or to a saga. Powder's actions are written in the boilerplate-busting syntax of [Gnar Edge Redux](https://www.npmjs.com/package/gnar-edge#gnaractions).

- **reducers**: The reducers specify how the application's state changes in response to actions sent to the store. Powder's reducers are written in the boilerplate-annihilating syntax of [Gnar Edge Redux](https://www.npmjs.com/package/gnar-edge#gnarreducers).

- **configureStore**: Combines the root reducer (the default export from `reducers.js`) with the sagas, using the [Redux Saga Middleware](https://redux-saga.js.org). In development mode, hot module replacement is configured for the reducers and sagas.

##### src/js/redux/sagas

Powder's sagas (side-effects).

- **index**: Registers all the Saga action watchers and configures the Sagas to accept a context which currently consists of the browser history, passed in from the `BrowserRouter` via the `ProviderWithRouter`.

- **accountDetails**: Fetches account details for a logged in user (i.e. on browser refresh) and submits changes to a user's account details.

- **activate**: Submits an activation token to Piste. On a successful response, stores the returned jwt token in local storage, updates the store with the user's email address and userId, and redirects to `/set-password`.

- **login**: Submits a login request and, on success, updates the store with the user's account details from the response.

- **logout**: Clears local storage (where the jwt token is stored), removes the user's account details from the store, and redirects to `/login`.

- **password**: The password-related sagas are stored in their own folder, to improve organization.

  - **countPwned**: Produces a SHA-1 hash of the current password, submits a request to the [Pwned Passwords Range API](https://haveibeenpwned.com/API/v2#SearchingPwnedPasswordsByRange) using the first 5 characters of the hash (preserves the value of the password), counts the number of matches to the full hash in the response, and updates the store with the match count.

  - **reset**: Submits a password reset request to Piste and, on success, notifies the user of the success and redirects to `/login`.

  - **score**: Submits a password score request to Piste and, on success, updates the store with the score and, if the score is 4, dispatches a `GET_PWNED_PASSWORD_COUNT` action.

  - **set**: This saga is run the first time a user sets their password, immediately after activating their account. On success, we redirect to `/account`.

- **sendResetPasswordEmail**: Submits a request to Piste to send a password reset email.

- **signup**: Submits a request to Piste to send an activation email and forwards to `/login`.

#### src/js/router

The application root and router.

- **App**: Defines the root structure of the app using the following components:

  - **BrowserRouter**: A `<Router>` that uses the HTML5 history API (pushState, replaceState and the popstate event) to keep your UI in sync with the URL.

  - **ProviderWithRouter**: See below.

  - **[CssBaseline](https://material-ui.com/style/css-baseline/)**: Normalizes our CSS.

  - **[MuiThemeProvider](https://material-ui.com/api/mui-theme-provider/)**: Provides a Material UI theme to all the views of the app.

  - **I18nButton**: Displays the Change Language button.

- **authenticatedRoute**: Dynamically loads a view from `views/dynamic`. Retrieves the jwt token from local storage and authenticates the user. If the token doesn't exist or has expired, redirects the user to `/login`.

- **providerWithRouter**: Configures Powder's store and returns a [Provider](https://redux.js.org/basics/usagewithreact#passing-the-store) wrapped in [withRouter](https://reacttraining.com/react-router/web/api/withRouter) higher-order component.

- **routes**: Defines Powder's base layout and router. Associates routes with their respective views using a [Switch](https://reacttraining.com/react-router/web/api/Switch).

#### src/js/util

- **index**: Registers all the app's utilities. Fun fact: Most of Edge's code started here.

- **handleApiError**: A generator function used by several sagas to temporarily add an error message to the store. Allows the associated component to briefly display the error message within its DOM.

#### src/js/views

Contains all of Powder's views.

- **accountDetails**: Consists of a few sub pages but primarily displays the address form.

  ![Account Details View](https://s3-us-west-2.amazonaws.com/powder-readme-assets/address-form.gif)

- **activate**: Simple view which briefly displays the message shown below and then redirects to `/set-password`.

  ![Activate View](https://s3-us-west-2.amazonaws.com/powder-readme-assets/activate.png)

- **dynamic**: Register dynamic (lazy load) views in this folder.
  - Add a file with same name as the view you want to make dynamic and export the view.
  - Use the file name as the `viewName` property of the associated `authenticatedRoute` in `routes.jsx`.

- **login**: Login form [shown above](#srcjscomponents).

- **sendPasswordResetEmail**: Accepts an email address and provides feedback to the user.

  ![Send Reset Password Email View](https://s3-us-west-2.amazonaws.com/powder-readme-assets/send-reset-password-email.gif)

- **setPassword**: This view handles the initial password setting as well as password resets. Passwords must score a 4 / 4 on the [zxcvbn scale](https://github.com/dropbox/zxcvbn) and not match a [pwned password](https://haveibeenpwned.com/Passwords).

  ![Reset Password View](https://s3-us-west-2.amazonaws.com/powder-readme-assets/reset-password.gif)

- **signup**: The initial signup view.

  ![Signup View](https://s3-us-west-2.amazonaws.com/powder-readme-assets/signup.png)

## Webpack Build

Powder is built with [Webpack 4](https://medium.com/webpack/webpack-4-released-today-6cdb994702d4). The build is configured in `webpack.config.babel.js` which is written in ES6. The `.babel.js` file extension is [recognized by webpack cli](https://github.com/webpack/webpack-cli/blob/368e2640e62c57eba4dde8b13c4c4470a293047c/bin/convert-argv.js#L141) and passed to [`@babel/register`](https://github.com/gulpjs/interpret/blob/master/index.js#L4) for transpilation to ES5 before the webpack run.

### Config

- **devServer**: Configures the development server to run on localhost:3000
  - historyApiFallback: Enables [History API Fallback](https://github.com/bripkens/connect-history-api-fallback) support (serves  index.html if a requested resource cannot be found)
- **devtool**: Source map specification - requires `optimization.minimizer.UglifyJsPlugin.sourceMap: true`
- **entry**: The main entry point. Code splitting is handled by the the `AuthenticatedRoute` component in `src/js/router`
- **mode**: Set to either `development` or `production` based on the environment
- **module.rules**:
  - **font**: Inlines fonts <= 8 KB, adds fonts > 8 KB to the build assets (applies to Roboto fonts from the `roboto-fontface` package)
  - **image**: Inlines imgaes <= 8 KB, adds images > 8 KB to the build assets
  - **js**: Transpiles all .js and .jsx files using Babel
  - **scss**: Applies the following sequence of transformations to the .scss files:
      - [sass-resources-loader](https://github.com/shakacode/sass-resources-loader): Handles `@import` directives
      - [sass-loader](https://github.com/webpack-contrib/sass-loader): Compiles Sass to CSS
      - [resolve-url-loader](https://www.npmjs.com/package/resolve-url-loader): Resolves relative paths in url() statements
      - [postcss-loader](https://github.com/postcss/postcss-loader): CSS transformation via plugins
      - [autoprefixer plugin](https://github.com/postcss/autoprefixer): Adds vendor prefixes to satisfy `browserslist` in `package.json`
      - [css-loader](https://github.com/webpack-contrib/css-loader): Converts  `@import` and `url()` to `import/require()`
      - [style-loader](https://www.npmjs.com/package/style-loader): Adds CSS to the DOM by injecting a `<style>` tag
  - **txt**: Raw loading of `.md` and `.txt` files - used to import `credit.txt`
- **optimization**: Optimizations to perform on the build
  - **minimizer**: Plugins to use to minimize the build
      - [OptimizeCSSAssetsPlugin](https://www.npmjs.com/package/optimize-css-assets-webpack-plugin): Uses [cssnano](https://cssnano.co) to minimize the CSS produced from the scss module
      - [UglifyJsPlugin](https://www.npmjs.com/package/uglifyjs-webpack-plugin): Mangles & compresses the js files produces from the js module.
  - **splitChunks**: Defines rules on how to split up the dependencies into separate bundles of the build. Each item in the array is used to produce a unique RegExp within the `node_modules` folder. Note that our application code is split up via the dynamic imports in `AuthenticatedRoute`.
- **output**: Defines the location of the build assets (`/dist`) and the naming convention for the build bundles.
- **performance**: Increases the default asset and entrypoint sizes (before warning) - the defaults are onorously small.
- **plugins**: Plugins to help with the build.
  - [EnvironmentPlugin](https://webpack.js.org/plugins/environment-plugin): Adds environment variables into the build using a find and replace on the pattern `process.env.${<< environment variable >>}`.
      - NODE_ENV: `development` or `production`.
      - ORIGINS: A map of services to origins for use in development, typically to separate ports on localhost. Used in `js/api/request.js`.
      - RECAPTCHA_SITE_KEY: Your Google reCAPTCHA site key.
  - [FaviconsWebpackPlugin](https://www.npmjs.com/package/favicons-webpack-plugin): Generates favicons for the build.
  - [IgnorePlugin](https://webpack.js.org/plugins/ignore-plugin): Used to remove moment locale files from the build.
  - [ProvidePlugin](https://webpack.js.org/plugins/provide-plugin): Used to automatically loads lodash (no import required).
  - [HtmlWebpackPlugin](https://www.npmjs.com/package/html-webpack-plugin): Builds the app's `index.html` file based on `assets/templates/index.htm`
  - [MiniCssExtractPlugin](https://www.npmjs.com/package/mini-css-extract-plugin): Extracts CSS into separate files (one per js bundle that uses CSS).
  - [BundleAnalyzerPlugin](https://www.npmjs.com/package/webpack-bundle-analyzer): Displays a visualization of the bundle analysis.
- **profile**: Boolean - Captures a profile of the build for use with the `BundleAnalyzerPlugin`.
- **resolve.extensions**: Allows use to omit `.js` and `.jsx` from imports.

## Linting

We use [eslint](https://eslint.org/) to lint the JavaScript files and [sass-lint](https://www.npmjs.com/package/sass-lint) to lint the SCSS files.

### Eslint

Powder's eslint config extends the fantastic [arirbnb config](https://github.com/airbnb/javascript) with ~ 40 rules where my own personal coding style differs from the airbnb base. A couple of examples: I'm not a fan of the dangling comma and the max line length is 120 characters (my MacBook resolution is set to 2560 x 1600 via [RDM](https://github.com/avibrazil/RDM)).

#### Eslint Config

- **env**: Allows global variables from `browser`, `es6`, `jasmine`, and `jest`.
- **extends**: Includes all [airbnb base rules](https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb-base/rules) and [react rules](https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb/rules).
- **globals**: Lodash is our only global via the webpack `ProvidePlugin`.
- **parser**: The `babel-eslint` parser is a wrapper around the Babel parser that makes it compatible with ESLint.
- **parserOptions**: Options are set for syntax compatibility with JSX, ES6 modules, and the [TC39 decorator proposal](https://github.com/tc39/proposal-decorators).
- **rules**: Overrides to the airbnb base rules.
- **settings**: The `import/resolver` enables eslint to resolve the aliases in `babel.config.js`

## Testing

Powder is unit tested with [Jest](https://jestjs.io) and [Enzyme](https://github.com/airbnb/enzyme). The project has 100% coverage, so there are plenty of examples of how to test the various types of code in the app.

### Jest Config

- **collectCoverage**: Set to `true` to enable coverage collection.
- **coveragePathIgnorePatterns**: Configured to ignore `node_modules` and all the `i18n` files aside from the english file.
- **coverageReporters**: Generates an `lcov` report (uploaded to codecov in the CI runs and accessible at `/coverage/lcov-report/index.html`) and a `txt` report (shown in the terminal).
- **coverageThreshold**: Set to 100% for `branches`, `functions`, `lines`, and `statements` - new code should require full coverage!
- **moduleNameMapper**: Points non-code files to mocks.
- **roots**: Defines our code root - Jest searches for tests here and also looks for manual mocks of modules in `node_modules` in the `__mocks__` folder adjacent to the project root.
- **setupFiles**: Points to `jest.setup.js` where we define a few globals and setup enzyme.
- **transformIgnorePatterns**: Defines the modules that aren't transpiled by Babel - configured to only transpile `gnar-edge` within `node_modules`.

---

<p><div align="center">Made with <img alt='Love' width='32' height='27' src='https://s3-us-west-2.amazonaws.com/project-gnar/heart-32x27.png'> by <a href='https://www.linkedin.com/in/briengivens'>Brien Givens</a></div></p>
