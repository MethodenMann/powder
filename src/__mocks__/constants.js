import Chance from 'chance';
import base64 from 'gnar-edge/es/base64';

import { CANADA, USA } from 'views/accountDetails/states';

const chance = new Chance();

export const accessToken = chance.guid({ version: 4 });
export const actionId = chance.guid({ version: 4 });
export const email = chance.email();
export const expiredJwt =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1MTQ3OTM2MDB9.kfvw3tFqLeCSVSHAVfCXrIVj9mUlmaFPotTI62phWsY';
export const handleChange =
  (() => {
    const names = {};
    return name => _.update(names, name, val => val ||
      jest.fn(e => (_.has(e, 'target') ? e.target.checked || e.target.value : e)))[name];
  })();
export const jwt =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjQxMDI0NzM2MDB9.Ew-x0Vz1kQQqsDpYDIvFXMJ72kGoNu8EqctZTQ5lM1U';
export const password = chance.string({ length: chance.integer({ max: 30, min: 8 }) });
export const reCaptchaResponse = 'Mock ReCAPTCHA Response';
export const token = base64.encode(`${email}/${actionId}`);
export const userId = chance.guid({ version: 4 });

const country = chance.bool() ? CANADA : USA;
const address1 = chance.address();
const address2 = chance.bool() ? '' : `Apt ${chance.integer({ max: 1000, min: 1 })}`;
const city = chance.city();
const firstName = chance.first();
const lastName = chance.last();
const gender = chance.integer({ max: 2, min: 0 });
const postalCode = country === CANADA ? chance.postal() : chance.zip();
const state = country === CANADA ? chance.province() : chance.state();


export const accountDetails = {
  userId,
  email,
  firstName,
  lastName,
  gender,
  address1,
  address2,
  city,
  state,
  postalCode
};

export const data = {
  firstName,
  lastName,
  gender,
  address1,
  address2,
  city,
  state,
  postalCode
};
