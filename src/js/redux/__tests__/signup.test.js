import { drain } from 'gnar-edge';

import { FetchMock } from 'mocks';
import { email, reCaptchaResponse } from 'mocks/constants';
import actions from 'actions';
import configureStore from 'js/redux/configureStore';

const { signupActions } = actions;

describe('redux activate', () => {
  it('submits the proper request on submitSignupEmail', drain(function* () {
    const store = configureStore();
    const fetchMock = new FetchMock({});
    yield store.dispatch(signupActions.submitSignupEmail(email, reCaptchaResponse));
    const body = JSON.stringify({ email, reCaptchaResponse });
    expect(fetchMock.fn).toHaveBeenCalledWith('/user/signup', { method: 'POST', body });
    fetchMock.deactivate();
  }));
});
