import { Map } from 'immutable';
import { StaticRouter } from 'react-router';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import React from 'react';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';
import createMuiTheme from '@material-ui/core/styles/createMuiTheme';

import { ENGLISH, english } from 'i18n';
import { SUBMIT_ACTIVATION } from 'actions';
import { matchMock } from 'mocks';
import { token } from 'mocks/constants';
import ActivateView from 'views/activate';

const { activate: { defaultContent, errorContent } } = english;

const mockStore = configureStore();
const muiTheme = createMuiTheme({
  typography: {
    useNextVariants: true
  }
});

const getRootComponent = initialState => {
  const store = mockStore({ activate: Map(), language: Map({ current: ENGLISH }), ...initialState });
  const routeMatch = _.chain(matchMock).cloneDeep().set('params.token', token).value();
  const root = mount((
    <StaticRouter context={{}}>
      <MuiThemeProvider theme={muiTheme}>
        <ActivateView match={routeMatch} store={store} />
      </MuiThemeProvider>
    </StaticRouter>
  ));
  return { root, store };
};

describe('<ActivateView />', () => {
  it('renders properly', () => {
    const { root } = getRootComponent();
    const contentDiv = root.find('Grid[item=true]');
    expect(contentDiv).toHaveLength(1);
    expect(contentDiv.text()).toBe(defaultContent.join(''));
  });

  it('dispatches the correct action when the component mounts', () => {
    const { store } = getRootComponent();
    expect(store.getActions()).toEqual([ { type: SUBMIT_ACTIVATION, payload: { token } } ]);
  });

  it('displays an error message when passed in from the store', () => {
    const error = 'Activation rejected by server.';
    const { root } = getRootComponent({ activate: Map({ error }) });
    expect(root.find('Grid[item=true]').text()).toBe(errorContent(error).join(''));
  });
});
