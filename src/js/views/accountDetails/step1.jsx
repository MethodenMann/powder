import FormControl from '@material-ui/core/FormControl';
import Grid from '@material-ui/core/Grid';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import canadaImg from 'assets/images/canada.png';
import i18n from 'i18n';
import states, { CANADA, USA } from './states';
import usaImg from 'assets/images/usa.png';

import './step1.scss';

export default class AccountDetailsStep1 extends Component {
  static propTypes = {
    activation: PropTypes.bool.isRequired,
    address1: PropTypes.string.isRequired,
    address2: PropTypes.string.isRequired,
    city: PropTypes.string.isRequired,
    firstName: PropTypes.string.isRequired,
    gender: PropTypes.number.isRequired,
    handleChange: PropTypes.func.isRequired,
    language: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    postalCode: PropTypes.string.isRequired,
    state: PropTypes.string.isRequired
  };

  handleAddressStateChange = e => {
    const input = e.target.value;
    const comparator = /^[A-Z]{2}$/.test(input) ? 'value' : 'text';
    const { value } = states.find(state => state[comparator] === input) || { value: '' };
    this.props.handleChange('state')({ target: { value } });
  };

  render() {
    const {
      activation,
      address1,
      address2,
      city,
      firstName,
      handleChange,
      language,
      lastName,
      postalCode,
      state,
      gender
    } = this.props;
    const {
      accountDetails: { step1Greeting },
      fieldLabels: {
        address1Label,
        address2Label,
        cityLabel,
        firstNameLabel,
        lastNameLabel,
        postalCodeLabel,
        provinceLabel,
        provinceStateLabel,
        stateLabel,
        zipCodeLabel,
        genderLabel
      }
    } = i18n[language];
    const stateObj = states.find(({ value }) => value === state) || {};
    return (
      <Grid container alignContent='flex-start' className='account-details-step-1' spacing={24}>
        {activation
          ? (
            <Grid item xs={12}>
              <Typography align='center' variant='h5'>{step1Greeting}</Typography>
            </Grid>
          )
          : null}
        <Grid item xs={6}>
          <TextField
            autoFocus
            fullWidth
            autoComplete='given-name'
            label={firstNameLabel}
            value={firstName}
            onChange={handleChange('firstName')}
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            fullWidth
            autoComplete='family-name'
            label={lastNameLabel}
            value={lastName}
            onChange={handleChange('lastName')}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            autoComplete='address-line1'
            label={address1Label}
            value={address1}
            onChange={handleChange('address1')}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            autoComplete='address-line2'
            label={address2Label}
            value={address2}
            onChange={handleChange('address2')}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            autoComplete='address-level2'
            label={cityLabel}
            value={city}
            onChange={handleChange('city')}
          />
        </Grid>
        <Grid item xs={8}>
          <div className='state-container'>
            <TextField
              fullWidth
              autoComplete='address-level1'
              className='state-input'
              InputProps={{ inputProps: { tabIndex: -1 } }}
              label=' '
              onChange={this.handleAddressStateChange}
            />
            <FormControl fullWidth className='state-select'>
              <InputLabel>
                {!state ? provinceStateLabel : stateObj.country === USA ? stateLabel : provinceLabel}
              </InputLabel>
              <Select fullWidth value={state} onChange={handleChange('state')}>
                {[ { text: <em>None</em>, value: '' }, ...states ].map(({ country, text, value }) => (
                  <MenuItem key={value} value={value}>
                    {country ? (
                      <img alt={country} className='flag' src={country === CANADA ? canadaImg : usaImg} />
                    ) : null}
                    {text}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </div>
        </Grid>
        <Grid item xs={4}>
          <TextField
            fullWidth
            autoComplete='postal-code'
            label={!state || stateObj.country === CANADA ? postalCodeLabel : zipCodeLabel}
            value={postalCode}
            onChange={handleChange('postalCode')}
          />
        </Grid>
        <Grid item xs={12}>
          <FormControl fullWidth>
            <InputLabel>{genderLabel}</InputLabel>
            <Select
              fullWidth
              autoComplete='gender'
              value={gender}
              onChange={handleChange('gender')}
            >
              <MenuItem value=''>
                <em>None</em>
              </MenuItem>
              <MenuItem value={0}>Male</MenuItem>
              <MenuItem value={1}>Female</MenuItem>
              <MenuItem value={2}>wont tell</MenuItem>
            </Select>
          </FormControl>
        </Grid>
      </Grid>
    );
  }
}
